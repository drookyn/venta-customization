Venta-Customization
===================

User-Scripts für das Fondsfinanz Venta-CRM.

### Anforderungen

1. [Google Chrome](https://www.google.de/chrome/browser/desktop/index.html)
2. [Tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=de)

### Installation (Windows)

```sh
$ cd pfad\zum\Projekt
$ .\installation\chrome.bat
```