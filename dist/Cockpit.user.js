// ==UserScript==
// @name         Venta - Cockpit
// @icon         https://www.fondsfinanz.de/images/favicon/xandroid-icon-192x192.png.pagespeed.ic.2m2HxNilXX.webp
// @namespace    https://flovolution.com/
// @match        https://www.fondsfinanz.de/venta-kvm/cockpit/cockpit.c1
// @version      1.0
// @description  Anpassung Cockpits von Venta
// @author       Florian Vogt
// @run-at       document-start
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @downloadURL  https://gitlab.com/AitzDieAmaise/venta-customization/raw/master/dist/Cockpit.user.js
// ==/UserScript==
'use strict';function _toConsumableArray(arr){if(Array.isArray(arr)){for(var i=0,arr2=Array(arr.length);i<arr.length;i++){arr2[i]=arr[i]}return arr2}else{return Array.from(arr)}}(function($){function convertDateEn(dateString){var _ref=arguments.length>1&&arguments[1]!==undefined?arguments[1]:{},replaceYear=_ref.replaceYear;var dateParts=dateString.split('.');if(replaceYear){dateParts[2]=replaceYear}return new Date(dateParts.reverse().join('-'))}function sortBirthdays(){var $birthdayTable=$('#crm-cockpit-geburtstage-cockpitGeburtstageTable-table');if($birthdayTable.length>0){var $tbody=$birthdayTable.find('tbody');var $rows=$tbody.find('tr');var dates=[];$rows.each(function(rowIndex,row){var $row=$(row);var $cell=$row.find('td:eq(1)');var date=$cell.find('span:eq(0)').text();var currentYear=new Date().getFullYear();var dayDiff=Math.round((convertDateEn(date,{replaceYear:currentYear})-new Date)/(1000*60*60*24));var absDayDiff=Math.abs(dayDiff);$cell.text($cell.text().replace(/(?!\d)+(\))/g,' '+(dayDiff<0?'vor':'in')+' '+absDayDiff+' '+(absDayDiff===1?'Tag':'Tagen')+')'));if(date){dates=[].concat(_toConsumableArray(dates),[{dayDiff:dayDiff,$row:$row}])}});$tbody.html(dates.sort(function(a,b){return b.dayDiff<a.dayDiff}).map(function(date){return date.$row}))}}$(document).ready(sortBirthdays)})(window.jQuery);