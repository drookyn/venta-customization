// ==UserScript==
// @name         Venta - Cockpit
// @icon         https://www.fondsfinanz.de/images/favicon/xandroid-icon-192x192.png.pagespeed.ic.2m2HxNilXX.webp
// @namespace    https://flovolution.com/
// @match        https://www.fondsfinanz.de/venta-kvm/cockpit/cockpit.c1
// @version      1.0
// @description  Anpassung Cockpits von Venta
// @author       Florian Vogt
// @run-at       document-start
// @require      https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @downloadURL  https://gitlab.com/AitzDieAmaise/venta-customization/raw/master/dist/Cockpit.user.js
// ==/UserScript==

'use strict';

($ => {
  function convertDateEn(dateString, { replaceYear } = {}) {
    const dateParts = dateString.split('.');

    if (replaceYear) {
      dateParts[2] = replaceYear;
    }

    return new Date(dateParts.reverse().join('-'));
  }

  function sortBirthdays() {
    const $birthdayTable = $(
      '#crm-cockpit-geburtstage-cockpitGeburtstageTable-table',
    );

    if ($birthdayTable.length > 0) {
      const $tbody = $birthdayTable.find('tbody');
      const $rows = $tbody.find('tr');
      let dates = [];

      $rows.each((rowIndex, row) => {
        const $row = $(row);
        const $cell = $row.find('td:eq(1)');
        const date = $cell.find('span:eq(0)').text();

        const currentYear = new Date().getFullYear();
        const dayDiff = Math.round(
          (convertDateEn(date, { replaceYear: currentYear }) - new Date()) /
            (1000 * 60 * 60 * 24),
        );
        const absDayDiff = Math.abs(dayDiff);

        $cell.text(
          $cell
            .text()
            .replace(
              /(?!\d)+(\))/g,
              ` ${dayDiff < 0 ? 'vor' : 'in'} ${absDayDiff} ${absDayDiff === 1
                ? 'Tag'
                : 'Tagen'})`,
            ),
        );

        if (date) {
          dates = [...dates, { dayDiff, $row }];
        }
      });

      $tbody.html(
        dates.sort((a, b) => b.dayDiff < a.dayDiff).map(date => date.$row),
      );
    }
  }

  $(document).ready(sortBirthdays);
})(window.jQuery);
